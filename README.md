# AudioGraderExample

An example deployment of the AudioGrader server (available at https://gitlab.fi.muni.cz/xpavlica/audiograder).

If you encounter any issues, consult the API README file at https://gitlab.fi.muni.cz/xpavlica/audiograder/-/blob/master/src/Api/README.MD